package de.ng.nizada.freebuild.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import de.ng.nizada.freebuild.Freebuild;

public class CommandPing implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(Freebuild.PREFIX + "§7Die Console hat keinen §4Ping§8.");
			return true;
		}
		sender.sendMessage(Freebuild.PREFIX + "§7Dein §aPing §7betr§gt §a" + ((CraftPlayer)sender).getHandle().ping + "§cms§8.");
		return true;
	}
}